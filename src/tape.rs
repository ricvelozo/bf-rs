// tape.rs
//
// Copyright 2021 Ricardo Silva Veloso <ricvelozo@gmail.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// https://www.apache.org/licenses/LICENSE-2.0> or the MIT License
// <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.
//
// SPDX-License-Identifier: (MIT OR Apache-2.0)

use std::num::Wrapping;

use crate::program::LogicalError;

/// A Brainfuck tape.
pub struct Tape {
    buf: Vec<Wrapping<i8>>,
    index: usize,
}

impl Tape {
    /// Constructs a new tape with 30,000 cells.
    #[inline]
    pub fn new() -> Self {
        Self::with_capacity(30_000)
    }

    /// Constructs a new tape with the specified capacity.
    #[inline]
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            buf: vec![Wrapping(0); capacity],
            index: 0,
        }
    }

    /// Returns a copy of the current cell value.
    #[inline]
    pub fn current_cell(&self) -> Wrapping<i8> {
        // SAFETY: `self.index` is already checked in `self.move_ptr()`.
        unsafe { *self.buf.get_unchecked(self.index) }
    }

    /// Returns a mutable reference to the current cell.
    #[inline]
    pub fn current_cell_mut(&mut self) -> &mut Wrapping<i8> {
        // SAFETY: `self.index` is already checked in `self.move_ptr()`.
        unsafe { &mut *self.buf.get_unchecked_mut(self.index) }
    }

    /// Advances or rewinds the tape. Will allocate more cells in the end if
    /// necessary, or will return [`LogicalError::BufOverflow`] if ocurrs a
    /// buffer overflow at the beginning.
    #[inline]
    pub fn move_ptr(&mut self, n: isize) -> Result<(), LogicalError> {
        self.index = self
            .index
            .checked_add_signed(n)
            .ok_or(LogicalError::BufOverflow)?;

        if self.index >= self.buf.len() {
            self.buf.resize(self.index * 2, Wrapping(0));
        }

        Ok(())
    }

    /// Addition at the current cell, wrapping around at the boundary of [`i8`].
    #[inline]
    pub fn calc(&mut self, x: Wrapping<i8>) {
        let cell = self.current_cell_mut();
        *cell += x;
    }
}
