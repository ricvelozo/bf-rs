// program.rs
//
// Copyright 2021 Ricardo Silva Veloso <ricvelozo@gmail.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// https://www.apache.org/licenses/LICENSE-2.0> or the MIT License
// <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.
//
// SPDX-License-Identifier: (MIT OR Apache-2.0)

use std::io::{BufReader, BufWriter, prelude::*};
use std::num::Wrapping;

use bumpalo::Bump;

use crate::program_io::ProgramIo;
use crate::tape::Tape;

/// A Brainfuck instruction.
#[derive(Debug)]
enum Instruction<'bump> {
    Move(isize),
    Add(Wrapping<i8>),
    Read,
    Write,
    Loop(&'bump [Instruction<'bump>]),
}

/// An error which can be returned when parsing an instruction.
#[derive(Debug)]
pub enum ParseError {
    LoopLeftOpen,
    NoLoopOpen,
}

/// An error which can be returned when running an instruction.
#[derive(Debug)]
pub enum LogicalError {
    BufOverflow,
}

/// A Brainfuck program.
pub struct Program<'bump, R: Read, W: Write> {
    instructions: Option<&'bump [Instruction<'bump>]>,
    pub(crate) io: ProgramIo<R, W>,
}

impl<'bump, R: Read, W: Write> Program<'bump, R, W> {
    /// Constructs a new program.
    #[inline]
    pub fn new(input: R, output: W) -> Self {
        Self {
            instructions: None,
            io: ProgramIo {
                input: BufReader::new(input),
                output: BufWriter::new(output),
            },
        }
    }

    /// Parses the instructions from an iterator.
    #[inline]
    pub fn parse(
        &mut self,
        arena: &'bump Bump,
        code: impl IntoIterator<Item = u8>,
    ) -> Result<(), ParseError> {
        let instructions = Self::parse_instructions(arena, &mut code.into_iter(), false)?;
        self.instructions = Some(instructions);
        Ok(())
    }

    fn parse_instructions(
        arena: &'bump Bump,
        iter: &mut impl Iterator<Item = u8>,
        mut open_loop: bool,
    ) -> Result<&'bump [Instruction<'bump>], ParseError> {
        // TODO: Use `std::collections::Vec::new_in()` (allocator_api #32838)
        let mut buf = bumpalo::collections::Vec::new_in(arena);

        while let Some(byte) = iter.next() {
            match byte {
                b'<' => match buf.last_mut() {
                    Some(Instruction::Move(last)) => *last -= 1,
                    _ => buf.push(Instruction::Move(-1)),
                },
                b'>' => match buf.last_mut() {
                    Some(Instruction::Move(last)) => *last += 1,
                    _ => buf.push(Instruction::Move(1)),
                },
                b'-' => match buf.last_mut() {
                    Some(Instruction::Add(last)) => *last -= 1,
                    _ => buf.push(Instruction::Add(Wrapping(-1))),
                },
                b'+' => match buf.last_mut() {
                    Some(Instruction::Add(last)) => *last += 1,
                    _ => buf.push(Instruction::Add(Wrapping(1))),
                },
                b',' => buf.push(Instruction::Read),
                b'.' => buf.push(Instruction::Write),
                b'[' => buf.push(Instruction::Loop(Self::parse_instructions(
                    arena, iter, true,
                )?)),
                b']' if open_loop => {
                    open_loop = false;
                    break;
                }
                b']' if !open_loop => return Err(ParseError::NoLoopOpen),
                _ => continue,
            }
        }

        if open_loop {
            return Err(ParseError::LoopLeftOpen);
        }

        Ok(buf.into_bump_slice())
    }

    /// Executes the program.
    #[inline]
    pub fn run(&mut self, tape: &mut Tape) -> Result<(), LogicalError> {
        let instructions = self.instructions.unwrap_or_default();
        Self::run_instructions(tape, &mut self.io, instructions)
    }

    fn run_instructions(
        tape: &mut Tape,
        io: &mut ProgramIo<R, W>,
        instructions: &[Instruction],
    ) -> Result<(), LogicalError> {
        for instruction in instructions {
            match instruction {
                Instruction::Move(n) => tape.move_ptr(*n)?,
                Instruction::Add(x) => tape.calc(*x),
                Instruction::Read => io.read(tape),
                Instruction::Write => io.write(tape),
                Instruction::Loop(instructions) => {
                    while tape.current_cell().0 > 0 {
                        Self::run_instructions(tape, io, instructions)?
                    }
                }
            }
        }

        Ok(())
    }
}
