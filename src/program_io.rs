// program_io.rs
//
// Copyright 2021 Ricardo Silva Veloso <ricvelozo@gmail.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// https://www.apache.org/licenses/LICENSE-2.0> or the MIT License
// <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.
//
// SPDX-License-Identifier: (MIT OR Apache-2.0)

use std::io::{BufReader, BufWriter, prelude::*};
use std::num::Wrapping;

use crate::tape::Tape;

/// The program's input/output handlers.
pub(crate) struct ProgramIo<R: Read, W: Write> {
    pub(crate) input: BufReader<R>,
    pub(crate) output: BufWriter<W>,
}

impl<R: Read, W: Write> ProgramIo<R, W> {
    /// Reads one byte from input and store in current cell.
    #[inline]
    pub fn read(&mut self, tape: &mut Tape) {
        // Flush the output before reading
        self.output.flush().expect("could not flush the output");

        let mut buf = [0];
        if self.input.read_exact(&mut buf).is_ok() {
            let cell = tape.current_cell_mut();
            *cell = Wrapping(buf[0] as i8);
        }
    }

    /// Writes the byte at current cell to output.
    #[inline]
    pub fn write(&mut self, tape: &mut Tape) {
        let cell = tape.current_cell();
        self.output
            .write_all(&[cell.0 as u8])
            .expect("could not write to the output");
    }
}
