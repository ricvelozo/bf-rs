// main.rs
//
// Copyright 2021 Ricardo Silva Veloso <ricvelozo@gmail.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// https://www.apache.org/licenses/LICENSE-2.0> or the MIT License
// <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.
//
// SPDX-License-Identifier: (MIT OR Apache-2.0)

use std::io;
use std::{env, fs};

use bumpalo::Bump;

use program::Program;
use tape::Tape;

mod program;
mod program_io;
mod tape;

#[cfg(feature = "mimalloc")]
#[global_allocator]
static GLOBAL: mimalloc::MiMalloc = mimalloc::MiMalloc;

fn main() {
    // Load the file.
    let Some(file_name) = env::args().nth(1) else {
        return;
    };

    let code = fs::read(file_name).expect("could not read file");
    let mut stdin = io::stdin().lock();
    let mut stdout = io::stdout().lock();
    let arena = Bump::new();

    // Parse the code.
    let mut program = Program::new(&mut stdin, &mut stdout);
    program
        .parse(&arena, code)
        .expect("a parser error occurred");

    // Run the program.
    let mut tape = Tape::new();
    program.run(&mut tape).expect("a logical error occurred");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_and_run_hello_world() {
        let code = include_bytes!("../data/hello.bf");
        let mut stdin = io::empty();
        let mut stdout = io::Cursor::new(Vec::new());
        let arena = Bump::new();

        // Parse the code.
        let mut program = Program::new(&mut stdin, &mut stdout);
        program.parse(&arena, *code).unwrap();

        // Run the program.
        let mut tape = Tape::new();
        program.run(&mut tape).unwrap();

        let output = program.io.output.into_inner().unwrap().clone().into_inner();
        assert_eq!(output, b"Hello World!\n");
    }
}
