# `bf`

Yet another Brainfuck interpreter in Rust.

## Usage

First, compile and install `bf` (requires Rust toolset):

```sh
cargo install
```

Then execute:

```sh
bf <file>
```

To uninstall:

```sh
cargo uninstall bf
```

## License

`bf` is licensed under either of the following, at your option:

*   Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
    https://www.apache.org/licenses/LICENSE-2.0)
*   MIT License ([LICENSE-MIT](LICENSE-MIT) or
    https://opensource.org/licenses/MIT)
